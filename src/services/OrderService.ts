import Vue from 'vue'
import AdressesLead from "@/models/AdressesLead";
import Lead from "@/models/Lead";
import Order from "@/models/Order";
import OrigemLead from "@/models/OrigemLead";
import StatusLead from "@/models/StatusLead";
import Tecnico from "@/models/Tecnico";
import Vendedor from "@/models/Vendedor";
import OrderInterface from "./OrderInterface";

class OrderService implements OrderInterface{
  mapObject(order: any) {
   return  new Order(
      order.id,
      order.area,
      order.consumo,
      order.corEtapaLead,
      new Date(order.dataAtualizacao).toLocaleString('pt-BR', { timeZone: 'UTC',dateStyle:'short' }),
      new Date(order.dataInclusao).toLocaleString('pt-BR', { timeZone: 'UTC',dateStyle:'short' }),
      order.descricao,
      new AdressesLead(
        order.enderecoLead.id,
        order.enderecoLead.cep,
        order.enderecoLead.logradouro,
        order.enderecoLead.numero,
        order.enderecoLead.complemento,
        order.enderecoLead.bairro,
        order.enderecoLead.cidade
      ),
      order.fluxo,
      order.etapaLead,
      order.imagem == null ? 'https://via.placeholder.com/140x100':order.imagem,
      order.imagemLead == null ? 'https://via.placeholder.com/140x100':order.imagemLead,
      new Lead(order.leadId,order.lead,this.setMaskCellPhone(order.leadCelular),order.leadEmail,order.tipoLead,new OrigemLead(order.origemLead.id,order.origemLead.nome,order.origemLead.statusLeadGatilho)),
      order.nome,
      order.prePropostaAceita,
      order.prePropostaAceitaId,
      order.prePropostaAceitaPotencia,
      order.prePropostaAceitaValor,
      order.previsaoFechamento,
      order.qtdFotos,
      order.qtdPrePropostas,
      order.qtdVendas,
      order.quantidadeUCs,
      new StatusLead(order.status.id,order.status.nome,order.status.ativo,order.status.excluido,order.status.cor),
      order.statusLead,
      new Tecnico(    order.tecnicoResponsavelId,order.tecnicoResponsavel),
      order.tipo,
      order.vendasValorTotal,
      new Vendedor(
      order.vendedorResponsavelId,
      order.vendedorResponsavel)

    )
  }
  setMaskCellPhone(cellPhone: string){
    return cellPhone.replace(/^(\d\d)(\d)/g,"($1) $2").replace(/(\d{5})(\d)/,"$1-$2");
  }

}
export default OrderService;
