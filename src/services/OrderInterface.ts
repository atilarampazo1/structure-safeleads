import Order from '@/models/Order';

export default interface OrderInterface {
  mapObject(order:any): Order;
}
