import AdressesLead from "./AdressesLead";
import Lead from "./Lead";
import OrigemLead from "./OrigemLead";
import StatusLead from "./StatusLead";
import Tecnico from "./Tecnico";
import Vendedor from "./Vendedor";

class Order {
  public id: Number;
  public area: Number;
  public consumo:Number;
  public corEtapaLead: any;
  public dataAtualizacao: string|null;
  public dataInclusao: string|null;
  public descricao: any;
  public enderecoLead: AdressesLead;
  public fluxo: Array<string>;
  public etapaLead: string;
  public imagem: string|null;
  public imagemLead: string|null;
  public lead: Lead;
  public nome: string;
  public prePropostaAceita: string;
  public prePropostaAceitaId: Number;
  public prePropostaAceitaPotencia: Number;
  public prePropostaAceitaValor: Number;
  public previsaoFechamento: string;
  public qtdFotos: Number;
  public qtdPrePropostas: Number;
  public qtdVendas: Number;
  public quantidadeUCs: Number;
  public status: StatusLead;
  public statusLead: string;
  public tecnico: Tecnico;
  public tipo: string;
  public vendasValorTotal: Number;
  public vendedor: Vendedor;


  constructor(
    id: Number,
    area: Number,
    consumo:Number,
    corEtapaLead: any,

    dataAtualizacao: string,
    dataInclusao: string,
    descricao: any,
    enderecoLead: AdressesLead,
    fluxo: Array<string>,
    etapaLead: string,
    imagem: string|null,
    imagemLead: string|null,
    lead: Lead,
    nome: string,
    prePropostaAceita: string,
    prePropostaAceitaId: Number,
    prePropostaAceitaPotencia: Number,
    prePropostaAceitaValor: Number,
    previsaoFechamento: string,
    qtdFotos: Number,
    qtdPrePropostas: Number,
    qtdVendas: Number,
    quantidadeUCs: Number,
    status: StatusLead,
    statusLead: string,
    tecnico: Tecnico,
    tipo: string,
    vendasValorTotal: Number,
    vendedor: Vendedor
  ) {
    this.id = id;
    this.area = area;
    this.consumo = consumo;
    this.corEtapaLead = corEtapaLead;
    this.dataAtualizacao = dataAtualizacao;
    this.dataInclusao = dataInclusao;
    this.descricao = descricao;
    this.enderecoLead = enderecoLead;
    this.fluxo = fluxo;
    this.etapaLead = etapaLead;
    this.imagem = imagem;
    this.imagemLead = imagemLead;
    this.lead = lead;
    this.nome = nome;
    this.prePropostaAceita = prePropostaAceita;
    this.prePropostaAceitaId = prePropostaAceitaId;
    this.prePropostaAceitaPotencia = prePropostaAceitaPotencia;
    this.prePropostaAceitaValor = prePropostaAceitaValor;
    this.previsaoFechamento = previsaoFechamento;
    this.qtdFotos = qtdFotos;
    this.qtdPrePropostas = qtdPrePropostas;
    this.qtdVendas = qtdVendas;
    this.quantidadeUCs = quantidadeUCs;
    this.status = status;
    this.statusLead = statusLead;
    this.tecnico = tecnico;
    this.tipo = tipo;

    this.vendasValorTotal = vendasValorTotal;
    this.vendedor = vendedor;
  }

}

export default Order;
