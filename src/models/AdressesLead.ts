class AdressesLead {
  public id: Number;
  public cep: any;
  public logradouro: string|null;
  public numero: string|null;
  public complemento: string|null;
  public bairro: string|null;
  public cidade: string|null;

  constructor(
    id: Number,
    cep: any,
    logradouro: string|null,
    numero: string|null,
    complemento: string|null,
    bairro: string|null,
    cidade: string|null
  ) {
    this.id = id;
    this.cep = cep;
    this.logradouro = logradouro;
    this.numero = numero;
    this.complemento = complemento;
    this.bairro = bairro;
    this.cidade = cidade;
  }
}
export default AdressesLead;
