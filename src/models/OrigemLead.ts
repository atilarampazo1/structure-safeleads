class OrigemLead {
  public id: Number;
  public nome: string;
  public statusLeadGatilho: any;

  constructor(
    id: Number,
    nome: string,
    statusLeadGatilho: any
  ) {
    this.id = id;
    this.nome = nome;
    this.statusLeadGatilho = statusLeadGatilho;
  }
}
export default OrigemLead;
