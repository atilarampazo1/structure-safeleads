import OrigemLead from "./OrigemLead";

class Lead {
  public id: Number;
  public nome: string | null;
  public celular: string | null;
  public email: string | null;
  public tipoLead: string | null;
  public origemLead: OrigemLead;
  constructor(id: Number, nome: string, celular: string, email: string, tipoLead: string,origemLead:OrigemLead) {
    this.id = id;
    this.nome = nome;
    this.celular = celular;
    this.email = email;
    this.tipoLead = tipoLead;
    this.origemLead = origemLead;
  }
}
export default Lead;
