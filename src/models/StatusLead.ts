class StatusLead {
  public id: Number;
  public nome: string;
  public ativo: boolean;
  public excluido: boolean;
  public cor: string;

  constructor(id: Number, nome: string, ativo: boolean, excluido: boolean, cor: string) {
    this.id = id;
    this.nome = nome;
    this.ativo = ativo;
    this.excluido = excluido;
    this.cor = cor;
  }
}
export default StatusLead;
